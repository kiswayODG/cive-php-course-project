Blog perso avec gestion et commentaires---------------------------------------
Url     : http://codes-sources.commentcamarche.net/source/33382-blog-perso-avec-gestion-et-commentairesAuteur  : cs_peter1789Date    : 12/08/2013
Licence :
=========

Ce document intitul� � Blog perso avec gestion et commentaires � issu de CommentCaMarche
(codes-sources.commentcamarche.net) est mis � disposition sous les termes de
la licence Creative Commons. Vous pouvez copier, modifier des copies de cette
source, dans les conditions fix�es par la licence, tant que cette note
appara�t clairement.

Description :
=============

c'est un blog compos&eacute; de 3 pages :
<br /> # 1 page administrative ('admi
n.php' prot&eacute;g&eacute;e par pseudo et mots de passe) qui permet de g&eacut
e;rer votre blog (vous pouvez utiliser le bbcode qui est d&eacute;crit sur cette
 page) : vous pouvez ajouter, supprimmer, modifier et voir vos articles compos&e
acute;s d'un titre, d'une photo et d'une description, et les classer dans des ru
briques ;
<br /> # 1 page &quot;visiteurs&quot; ('blog.php') qui affiche les ar
ticles selon la rubrique choisie dans le menu en haut, pour chaque article on pe
ut &eacute;crire un commentaires ou voir ceux d&eacute;j&agrave; post&eacute;s ;

<br /> # 1 page &quot;commentaires&quot; ('coms.php') pour voir et &eacute;cri
re les commentaires.
<br />
<br />&sect;  choses &agrave; faire :
<br />-choi
sir vos pseudo et mot de passe dans &quot;admin.php&quot; ;
<br />-sp&eacute;ci
fier votre base de donn&eacute;es dans &quot;admin.php&quot; ;
<br />-sp&eacute
;cifier vos param&egrave;tres de connexion dans &quot;admin.php&quot; ;
<br />-
executer les 3 requ&egrave;tes mysql situ&eacute;es au d&eacute;but de &quot;adm
in.php&quot; (en commentaire).
<br />
<br />PS : je vous laisse am&eacute;lior
er le style avec la feuille de style &quot;style.css&quot; appliqu&eacute;e sur 
toutes les pages ...
<br /><a name='conclusion'></a><h2> Conclusion : </h2>
<
br />bugs connus : les tableaus ne se centre pas sur Mozilla Firefox (et sans do
ute Netscape) car il n'accepte pas l'attribut :
<br />style=&quot;text-align:ce
nter&quot;
<br />pour les tableaux !
<br />
<br />Je joins un zip qui contien
t les fichiers &quot;admin.php&quot;, &quot;blog.php&quot;, &quot;coms.php&quot;
 et &quot;style.css&quot;.
<br />
<br />           @+
