
<header class="header">
    <div class="header-inner">
        <div class="container">
            <div class="inner">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-12">

                        <div class="logo">
                            <a href="index.html"><img src="../resources/images/logo.png" alt="#"></a>
                        </div>
                        <div class="mobile-nav"></div>
                        <!-- End Mobile Nav -->
                    </div>
                    <div class="col-lg-7 col-md-9 col-12">

                        <div class="main-menu">
                            <nav class="navigation">
                                <ul class="nav menu">
                                    <li class="active"><a href="/">Home <i class="icofont-rounded-down"></i></a>
                                        <ul class="dropdown">
                                            <li><a href="http://sspu.edu.cn/">About SSPU</a></li>
                                            <li><a href="https://www.univ-bobo.gov.bf/accueil">About UNB</a></li>
                                            <li><a href="https://www.huawei.com/cn/">About HUAWEI</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Gallery<i class="icofont-rounded-down"></i></a>
                                        <ul class="dropdown">
                                            <li><a href="index.html">All</a></li>
                                            <li><a href="index.html">HUAWEI</a></li>
                                            <li><a href="index.html">SSPU</a></li>
                                        </ul></li>
                                    <li><a href="#">Articles <i class="icofont-rounded-down"></i></a></li>
                                    <li><a href="#">Events <i class="icofont-rounded-down"></i></a>
                                

                                    <li><a href="contact.html">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                    <div class="col-lg-2 col-12">
                        <div class="get-quote">
                            <a href="appointment.html" class="btn">Language: English</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>