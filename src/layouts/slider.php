<section class="slider">
    <div class="hero-slider">

    <div class="single-slider" style="background-image:url('resources/images/img1.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="text">
                           
                        <h1> <br><br> <br>Students of <span>Burkina Faso;</span> academic and cultural <span>journey in Shanghai!</span></h1>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single-slider" style="background-image:url('resources/images/img2.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="text">
                            <h1> <br><br> <br>We are enjoy to <span> live and discover</span> Shanghai education and <span>cultural!</span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="single-slider" style="background-image:url('resources/images/img3.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="text">
                            <h1><br><br> <br> We come from <span>Burkina Faso</span> and we are <span>students!</span></h1>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="schedule">
    <div class="container">
        <div class="schedule-inner">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 ">
                    
                    <div class="single-schedule first">
                        <div class="inner">
                            <div class="icon">
                                <i class="fa fa-ambulance"></i>
                            </div>
                            <div class="single-content">
                                <span>Partner</span>
                                <h4>SSPU</h4>
                                <p>Shanghai Second Polytechnic University (SSPU) is a municipal public university that boasts strengths in Engineering and well-coordinated development of disciplines in Management, Economics, Literature, Science and Arts</p>
                                <a href="http://sspu.edu.cn/">LEARN MORE<i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    
                    <div class="single-schedule middle">
                        <div class="inner">
                            <div class="icon">
                                <i class="icofont-prescription"></i>
                            </div>
                            <div class="single-content">
                                <span>Partner</span>
                                <h4>UNB</h4>
                                <p>Nazi Boni University, a public scientific, cultural and technical institution (EPSCT), is a major player in university education and research in Burkina Faso, with the main mission of teaching and training students.</p>
                                <a href="https://www.univ-bobo.gov.bf/accueil">LEARN MORE<i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-12">
                    <div class="single-schedule last">
                        <div class="inner">
                            <div class="icon">
                                <i class="icofont-ui-clock"></i>
                            </div>
                            <div class="single-content">
                                <span>Partner</span>
                                <h4>Huawei</h4>
                                <p>
                                Huawei's  ICT infrastructure converges device, pipe, and cloud technologies, working seamlessly to build up a digital ecosystem that promotes joint innovation, mutually shared resources, and mutually shared success.</p>
                                <a href="https://www.huawei.com/cn/">LEARN MORE<i class="fa fa-long-arrow-right"></i></a>          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="why-choose section" >
			<div class="container">
				<div class="row">
					<!-- <div class="col-lg-12">
						<div class="section-title">
							<h2>We Offer Different Services To Improve Your Health</h2>
							<img src="img/section-img.png" alt="#">
							<p>Lorem ipsum dolor sit amet consectetur adipiscing elit praesent aliquet. pretiumts</p>
						</div>
					</div> -->
				</div>
				<div class="row">
					<div class="col-lg-6 col-12">
						<!-- Start Choose Left -->
						<div class="choose-left">
							<h3>The Luban Workshop</h3>
							<p>Luban was an outstanding "craftsman and inventor" in China 2500 <> years ago, a model of handcrafted skills and inventions, and the founder of many industries such as construction and civil use, and was known as "the ancestor of a hundred craftsmen".
 </p>
							<p>The name Luban is not only a symbol of the great wisdom of the Chinese workers, but also represents</p>
							<div class="row">
								<div class="col-lg-6">
									<ul class="list">
										<li><i class="fa fa-caret-right"></i>Superb technical skills </li>
										<li><i class="fa fa-caret-right"></i>professionalism</li>
								
									</ul>
								</div>
								<div class="col-lg-6">
									<ul class="list">
										<li><i class="fa fa-caret-right"></i>Continuous Improvement </li>
										<li><i class="fa fa-caret-right"></i>Innovative spirit</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- End Choose Left -->
					</div>
					<div class="col-lg-6 col-12">
						<!-- Start Choose Rights -->
						<div class="choose-right">
							<div class="video-image">
								<!-- Video Animation -->
								<div class="promo-video">
									<!-- <div class="waves-block">
										<div class="waves wave-1"></div>
										<div class="waves wave-2"></div>
										<div class="waves wave-3"></div>
									</div> -->
								</div>
								<!--/ End Video Animation -->
                                <video autoplay muted loop style="width: 100%; height: 100%;">
                                    <source src="resources/images/Video.mp4" type="video/mp4" >
                                 </video>
							</div>
						</div>
						<!-- End Choose Rights -->
					</div>
				</div>
			</div>
		</section>